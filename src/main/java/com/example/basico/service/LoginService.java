package com.example.basico.service;

import com.example.basico.model.User;
import com.example.basico.repository.LoginRepository;
import com.example.basico.config.JWTVerifierImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    private LoginRepository loginRepository;
    private JWTVerifierImpl jwtVerifier;

    @Autowired
    public LoginService(LoginRepository loginRepository, JWTVerifierImpl jwtVerifier) {
        this.loginRepository = loginRepository;
        this.jwtVerifier = jwtVerifier;
    }

    public String registerUser(User user) {

        User existingUser = loginRepository.findByEmail(user.getEmail());
        if (existingUser != null) {
            return "Ya existe un usuario registrado con este email";
        }


        loginRepository.save(user);
        return "Usuario registrado exitosamente";
    }


    public String login(String email, String password) {
        User user = loginRepository.findByEmail(email);
        if (user != null && user.getPassword().equals(password)) {

            String token = generateJWTToken(user);
            return token;
        } else {
            return null;
        }
    }


    public String logout() {

        return "Cierre de sesión exitoso (elimina el token del lado del cliente)";
    }


    private String generateJWTToken(User user) {


        String token = jwtVerifier.generateToken(user);
        return token;
    }
}
