package com.example.basico.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.example.basico.model.User; // Asegúrate de importar la clase User

import java.util.Date; // Importa la clase Date para trabajar con fechas

public class JWTVerifierImpl implements JWTVerifier {

    private String secret;


    private static final long EXPIRATION_TIME = 86400000; // 24 horas en milisegundos

    public JWTVerifierImpl(String secret) {
        this.secret = secret;
    }

    @Override
    public DecodedJWT verify(String token) throws JWTVerificationException {
        return JWT.require(Algorithm.HMAC256(secret))
                .withIssuer("my-issuer")
                .build()
                .verify(token);
    }

    @Override
    public DecodedJWT verify(DecodedJWT decodedJWT) throws JWTVerificationException {
        return JWT.require(Algorithm.HMAC256(secret))
                .withIssuer("my-issuer")
                .build()
                .verify(decodedJWT);
    }

    // Método para generar un token JWT
    public String generateToken(User user) {
        String token = JWT.create()
                .withClaim("userEmail", user.getEmail())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC256(secret));
        return token;
    }
}

