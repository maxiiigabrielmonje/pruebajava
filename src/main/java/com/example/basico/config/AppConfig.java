package com.example.basico.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    // Define un bean de tipo JWTVerifierImpl
    @Bean
    public JWTVerifierImpl jwtVerifier() {
        // Aquí puedes proporcionar el secret que deseas utilizar
        String secret = "mi-secreto";
        return new JWTVerifierImpl(secret);
    }
}
