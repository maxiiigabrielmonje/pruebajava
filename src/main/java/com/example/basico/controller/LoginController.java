package com.example.basico.controller;

import com.example.basico.model.User;
import com.example.basico.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class LoginController {

    private LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/register")
    public String registerUser(@RequestBody User user) {
        return loginService.registerUser(user);
    }

    @PostMapping("/login")
    public String login(@RequestBody User user) {
        return loginService.login(user.getEmail(), user.getPassword());
    }

    @PostMapping("/logout")
    public String logout() {
        return loginService.logout();
    }
}
